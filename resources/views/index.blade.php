@extends("layout.master")

@section("content")
		<section id="content">
			<h2>Veja as empresas apoiadas pela Academia do Pintor em seu Estado.</h2>
			<p class="italic">Entre em contato diretamente com as empresas parceiras para saber datas e locais dos cursos.</p>
			<form action="">
				<select name="select" id="select">
					<option value="">Selecione seu Estado</option>
					<option value="SP">SP</option>
					<option value="MG">MG</option>
					<option value="SC">SC</option>
					<option value="RJ">RJ</option>
					<option value="DF">DF</option>
					<option value="SC">SC</option>
					<option value="GO">GO</option>
					<option value="PE">PE</option>
					<option value="BA">BA</option>
					<option value="AL">AL</option>
					<option value="PI">PI</option>
					<option value="PR">PR</option>
					<option value="MS">MS</option>
				</select>
			</form>
			<div id="divisor"></div>
			<h3></h3>
			<!-- SP -->
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO </span>
				<span class="address">Av. dos Expedicionários, 30 Centro - Campinas/SP - CEP: 13013-140</span>
				<span class="phone">(19) 2515-4050</span>
				<span class="email"><a href="mailto:campinas.centro@escolaconcretta.com.br">campinas.centro@escolaconcretta.com.br</a></span>
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">AÇÃO FORTE</span>
				<span class="address">Rua Benedita Arruda Prado, 111 - Parque Vila Norte - Campinas/SP - CEP: 13065-380</span>
				<span class="phone">(19) 3245-2451</span>				
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">BM&F BOVESPA</span>
				<span class="address">Rua Monsenhor de Andrade, 331/341 - Bras - São Paulo/SP - CEP: 03008-000</span>
				<span class="phone">(11) 3229-9033</span>
				<span class="email"><a href="mailto:apbmf@bvmf.com.br">apbmf@bvmf.com.br</a></span>
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">EDUCAÇÃO NA CONSTRUÇÃO CIVIL - CUPECÊ - LTDA. </span>
				<span class="address">Rua Públio Pimentel, 25 (Esquina com Av. Cupecê Nº 3888) Cidade Ademar - São Paulo/ SP - CEP 04408-000</span>
				<span class="phone">(11) 98198- 7862</span>
				<span class="email"><a href="mailto:apbmf@bvmf.com.br">sp.jabaquara@escolaconcretta.com.br</a></span>
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">WALTER SERES JUNIOR ME </span>
				<span class="address">Av. Doutor Timóteo Penteado, 67 - Vila Hilda - Guarulhos/SP - CEP: 07094-000</span>
				<span class="phone">(11) 947928480</span>
				<span class="email"><a href="mailto:guarulhos.sp@escolaconcretta.com.br">guarulhos.sp@escolaconcretta.com.br</a></span>
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">EMEI CELINA POLCI</span>
				<span class="address">Rua Barretos, 217, Baeta Neves - São Bernardo do Campo/SP - CEP: 09751-450</span>
				<span class="phone">(11) 97821-9149 / (11) 4122-1605</span>			
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">EGERIA ENSINO PROFISSIONALI</span>
				<span class="address">Rua Cel Joaquim Antônio Dias, 123A  - Tatuapé/SP - CEP 03308-030</span>
				<span class="phone">(11) 99767-3270 / (11)98429-6402</span>		
				<span class="email"><a href="mailto:tatuape.sp@escolaconcretta.com.br">tatuape.sp@escolaconcretta.com.br</a></span>	
			</div>
			<div class="SP box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua Pedro Bellegarde, 327- Tatuapé/SP - CEP: 03317-080</span>
				<span class="phone">(11) 5011-4342</span>		
			</div>		

			<!-- SC -->
			<div class="SC box animated fadeInDown hide">
				<span class="enterprise">SOMMER E TORRENS LTDA - ME. </span>
				<span class="address">Praça 7 de setembro, 22 - Centro - Palhoça/SC - CEP: 88130-200</span>
				<span class="phone">(48) 3093-7373/3038-2380</span>
				<span class="email"><a href="mailto:master.sul@escolaconcretta.com.br">master.sul@escolaconcretta.com.br</a></span>
			</div>

			<!-- RJ -->
			<div class="RJ box animated fadeInDown hide">
				<span class="enterprise">TOP TEAM</span>
				<span class="address">Rua Ana Neri, 236  - Mangueira/RJ - CEP: 20960-081</span>
				<span class="phone">(21) 3040-0400</span>
			</div>
			<div class="RJ box animated fadeInDown hide">
				<span class="enterprise">FGMT SERVIÇOS DE CURSOS PROFISSIONALIZANTES LTDA</span>
				<span class="address">Av. Isabel, 22 - Santa Cruz – Rio de Janeiro/RJ - CEP: 23515-161 </span>
				<span class="phone">(21) 3395-5554/ 98247-6497</span>
				<span class="email"><a href="mailto:rj.santacruz@escolaconcretta.com.br"> rj.santacruz@escolaconcretta.com.br</a></span>
			</div>
			<div class="RJ box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Estrada dos Bandeirantes, 4457 - Rio de Janeiro/RJ - CEP 27775-113</span>
				<span class="phone">(21) 3413-5319</span>
			</div>
			<div class="RJ box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Av. Isabel, 22 - Santa Cruz/RJ - CEP: 23510-045</span>
				<span class="phone">(21) 3395-5554</span>
			</div>
			<div class="RJ box animated fadeInDown hide">
				<span class="enterprise">BRIZOT & DUARTE TREINAMENTO EM DESENVOLVIMENTO PROFISSIONAL E GERENCIAL LTDA - ME</span>
				<span class="address">Rua Maria Freitas, 77 - 2º, 3º e 4º - Madureira/RJ - CEP: 21351-010</span>
				<span class="phone">(21) 2452-1310 / (21)98349-3909</span>
			</div>

			<!-- MG -->
			<div class="MG box animated fadeInDown hide">
				<span class="enterprise">LUCIANA SOUSA CAMARGOS ME </span>
				<span class="address">Avenida João Cesar de Oliveira, 2605 1° Andar - Bairro Eldorado - Contagem/MG - CEP: 32315-000</span>
				<span class="phone">(31) 3391-5291 / (31) 8455-7071</span>
				<span class="email"><a href="mailto:contagem@escolaconcretta.com.br">contagem@escolaconcretta.com.br</a></span>
			</div>

			<div class="MG box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO </span>
				<span class="address">Avenida João Cesar de Oliveira, 2605 1° Andar - Bairro Eldorado - Contagem/MG - CEP: 32315-000</span>
				<span class="phone">(31) 3389-8514</span>
			</div>			

			<!-- DF -->
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Via Estado Adonir Guimarães, QD. 04, Conj. J LT 59/60  - 3º andar - Vila Buritis - Planaltina/DF - CEP: 73360-410</span>
				<span class="phone">(61) 3051-1654</span>
				<span class="email"><a href="mailto:planaltina.df@escolaconcretta.com.br">planaltina.df@escolaconcretta.com.br</a></span>
			</div>
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">QD QS 404 Conjunto A LT. 02 - Loja 2 - Samambaia Norte - Samambaia/DF - CEP: 72318-551</span>
				<span class="phone">(61) 3049-0053 / (61) 3027-4141</span>
				<span class="email"><a href="mailto:samambaia@escolaconcretta.com.br">samambaia@escolaconcretta.com.br</a></span>
			</div>
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">QSD 43 LT. 19 - Pistão Sul de Taguatinga - Brasilia/DF - CEP: 72020-430</span>
				<span class="phone">(61) 3049-0053 / (61) 3027-4141</span>
				<span class="email"><a href="mailto:taguatingasul@escolaconcretta.com.br">taguatingasul@escolaconcretta.com.br</a></span>
			</div>
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua 21 QD. 62, LT. 15 - Sobre-Loja - Jardim Oriente - Valparaíso/GO - CEP: 72876-221</span>
				<span class="phone">(61) 3049-0053 / (61) 3027-4141</span>
				<span class="email"><a href="mailto:expansao@escolaconcretta.com.br">expansao@escolaconcretta.com.br</a></span>
			</div>
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Av Comercial Lote 1711 Loja 01 e 02 Centro - São Sebastião/DF</span>
				<span class="phone">(61) 9202-9996</span>
				<span class="email"><a href="mailto:saosebastiao@escolaconcretta.com.br">saosebastiao@escolaconcretta.com.br</a></span>
			</div>
			<div class="DF box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Cl 115 lote A, 4° pavimento - Santa Maria/DF</span>
				<span class="phone">(61) 8122-0481</span>
				<span class="email"><a href="mailto:santamaria@escolaconcretta.com.br">santamaria@escolaconcretta.com.br</a></span>
			</div>

			<!-- GO -->
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">SARAIVA VERANO ESCOLA DE CONSTRUÇÃO LTDA - ME</span>
				<span class="address">Av.Jk QD. 12 - Jardim Brasília - Águas Lindas/GO - CEP: 72915-138</span>
				<span class="phone">(61) 9655- 4074</span>
				<span class="email"><a href="mailto:aguaslindas@escolaconcretta.com.br">aguaslindas@escolaconcretta.com.br</a></span>
			</div>			
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua 21 Quadra 62 Lote 15 Sobre-loja Bairro Jardim Oriente - Valparaíso/GO</span>
				<span class="phone">(61) 9618- 5757</span>
				<span class="email"><a href="mailto:valparaiso@escolaconcretta.com.br">valparaiso@escolaconcretta.com.br</a></span>
			</div>
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua José Viana Lobo Nº 320 (ao lado da AABB) Centro - Formosa/GO</span>
				<span class="phone">(61) 8462-7068</span>
				<span class="email"><a href="mailto:formosa@escolaconcretta.com.br">formosa@escolaconcretta.com.br</a></span>
			</div>
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA DE LUZIÂNIA</span>
				<span class="address">Rua 01 QD. 02 Lote 15 - Sala Térreo - Diogo Machado de Araújo - CEP: 72800-440</span>
				<span class="phone">(61) 3052-6222 - SRª Amalice</span>				
			</div>
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Rua Antônio de Sousa França QD. 9C, LT 22 - Filostro Machado - Anápolis/GO</span>
				<span class="phone">(62) 9230-1104 - Ricardo</span>
				<span class="email"><a href="mailto:canaa.senai@sistemafirg.org.br">canaa.senai@sistemafirg.org.br</a></span>
			</div>
			<div class="GO box animated fadeInDown hide">
				<span class="enterprise">GMB - SERVICOS DE INFORMACAO TELEFONICA, DIGITAIS, E INFORMATICA LTDA - ME</span>
				<span class="address">Rua 1 Quadra 2 Lt 15 - Térreo - Ed. Arthur, Centro - Luziânia /GO CEP: 72810-130</span>
				<span class="phone">(61) 3052-6222</span>
				<span class="email"><a href="mailto:luziania.go@escolaconcretta.com.br">luziania.go@escolaconcretta.com.br</a></span>
			</div>
			<!-- PE -->
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Av. Frei Cassimiro, 88 - Santo Amaro - Recife/PE - CEP: 14056-300</span>
				<span class="phone">(81) 3202-9300</span>
				<span class="email"><a href="mailto:renata.melo@pe.senai.br">renata.melo@pe.senai.br</a></span>
			</div>
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Av. 2º Travessa Pe. Oseas Cavalcante, 800 - Bairro Novo - Camaragibe/PE</span>
				<span class="phone">(81) 3202-9300</span>
				<span class="email"><a href="mailto:renata.melo@pe.senai.br">renata.melo@pe.senai.br</a></span>
			</div>
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Rua Alexandre Rodrigues Ferreira, 163 - Mangueira, Recife/PE - CEP: 50820-010</span>
				<span class="phone">(81) 3428-2600</span>
				<span class="email"><a href="mailto:renata.melo@pe.senai.br">renata.melo@pe.senai.br</a></span>
			</div>
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">SENAI</span>
				<span class="address">Av. Norte Miguel Arraes de Alencar, 539 - Santo Amaro, Recife/PE - CEP 50100-000</span>
				<span class="phone">(81) 3202-5122</span>
				<span class="email"><a href="mailto:renata.melo@pe.senai.br">renata.melo@pe.senai.br</a></span>
			</div>
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua Ulhoa Cintra 122, Santo Antonio, Recife/PE - CEP 50.010-020</span>
				<span class="phone">(81) 9184-1112</span>
				<span class="email"><a href="mailto:master.nordeste@escolaconcretta.com.br">master.nordeste@escolaconcretta.com.br</a></span>
			</div>
			<div class="PE box animated fadeInDown hide">
				<span class="enterprise">RJ EMPREENDIMENTOS EDUCACIONAIS LTDA</span>
				<span class="address">Rua Ulhoa Cintra 122, Santo Antonio, Recife/PE - CEP 50.010-020</span>
				<span class="phone">(81) 9184-1112</span>
				<span class="email"><a href="mailto:master.nordeste@escolaconcretta.com.br">master.nordeste@escolaconcretta.com.br</a></span>
			</div>

			<!-- BA -->
			<div class="BA box animated fadeInDown hide">
				<span class="enterprise"> A&A CURSOS PROFISSIONALIZANTES LTDA</span>
				<span class="address">Rua Padre Vieira, n° 172 - Centro - Alagoinhas/BA - CEP: 48040-320</span>
				<span class="phone">(71) 8145-2595</span>
				<span class="email"><a href="mailto:alagoinhas.ba@escolaconcretta.com.br">alagoinhas.ba@escolaconcretta.com.br</a></span>
			</div>
			<div class="BA box animated fadeInDown hide">
				<span class="enterprise">CENTRO EDUCACIONAL ALMEIDA NEVES LTDA. </span>
				<span class="address">Rua José Joaquim Seabra, 133, Centro - Feira de Santana/BA - CEP: 44.002-000</span>
				<span class="phone">(92) 8210-3456 / (71) 8211-2302</span>
				<span class="email"><a href="mailto:feiradesantana.ba@escolaconcretta.com.br">feiradesantana.ba@escolaconcretta.com.br / solon.almeida@iblojas.com.br / neto: hjsn.neto@hotmail.com / solon_almeida@hotmail.com</a></span>
			</div>
			<div class="BA box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Rua José Joaquim Seabra, 133 - Centro / Feira de Santana-Bahia</span>
				<span class="phone">(75) 3226-8924</span>
				<span class="email"><a href="mailto:feiradesantana.ba@escolaconcretta.com.br">feiradesantana.ba@escolaconcretta.com.br</a></span>
			</div>
			<div class="BA box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Av: São Cristovão, 17366 - Loja 01 - Bairro Itapuã - Salvador/BA - CEP: 41510-590</span>
				<span class="phone">(71) 9216-2126</span>
			</div>
			<div class="BA box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Av. Joana Angelica, 651 - Loja 04 - Centro - Salvador/BA - CEP: 40050-001</span>
				<span class="phone">(61) 3049-0053 / (61) 3027-4141</span>
			</div>												

		
			<!-- PI -->
			<div class="PI box animated fadeInDown hide">
				<span class="enterprise">GALUMA CURSOS E TREINAMENTOS DE EDUCACAO PROFISSIONAL LTDA ­ ME</span>
				<span class="address">Rua Desembargador Pires de Castro, 145 - Centro Sul - Teresina/PI - CEP 64002-490</span>
				<span class="phone">(86) 9991-1589/ (86) 9404-0055/ (86) 8822-5585/ (86) 9907-9636</span>
				<span class="email"><a href="mailto:teresina.centro@escolaconretta.com.br ">teresina.centro@escolaconretta.com.br </a></span>
			</div>

			<!-- AL -->
			<div class="AL box animated fadeInDown hide">
				<span class="enterprise">CONCRETTA ESCOLA DA CONSTRUÇÃO</span>
				<span class="address">Avenida Juca Sampaio N° 70, Barro Duro Maceió/AL - CEP 57045-365</span>
				<span class="phone">(82) 99922-8717 / (82) 99902- 6547</span>
				<span class="email"><a href="mailto:maceio.barroduro@escolaconcretta.com.br">maceio.barroduro@escolaconcretta.com.br</a></span>
			</div>

			<!-- PR -->
			<div class="PR box animated fadeInDown hide">
				<span class="enterprise">A.M. OFUCHI & CIA LTDA. </span>
				<span class="address">Av. Duque de Caxias, 370 Sobreloja - Centro-  Maringá/PR - CEP: 87013-180 </span>
				<span class="phone">(44) 9955- 4747/ (44) 9758-9999</span>
				<span class="email"><a href="mailto:maringa@escolaconcretta.com.br">maringa@escolaconcretta.com.br</a></span>
			</div>


			<!-- MS -->
			<div class="MS box animated fadeInDown hide">
				<span class="enterprise">RICARDO MAZZONI BOMFIM ME </span>
				<span class="address">Rua 13 de Maio N° 2499 Centro Campo Grande/MS</span>
				<span class="phone">(67) 8207- 1060</span>
				<span class="email"><a href="mailto:campogrande@escolaconcretta.com.br">campogrande@escolaconcretta.com.br</a></span>
			</div>

		</section>


@endsection

@section("scripts")
	$(document).ready(function () {
		var countform=0;
		$('#form-menu').on('hidden.bs.modal', function (e) {
		  $("input[type=text]").val("");
		  $("input[type=password]").val("");
		  $("textarea").val("");
		  $(".inbox").hide();
		  $(".inbox-rel").hide();
		  countform=0;
		});

		$('#form-menu').on('hidden.bs.modal', function (e) {
		  $("input[type=text]").val("");
		  $("input[type=password]").val("");
		  $("textarea").val("");
		  $(".inbox").hide();
		  $(".inbox-rel").hide();
		});

		function getCursos(){
			$.ajax({
			    headers: {
			        'X-CSRF-Token': $('meta[name="_token"]').val()
			    },
			    type: 'GET',
			    url: "{{ URL::route("getcurso") }}",
			    enctype: 'multipart/form-data',
			    success: function(data){
			    	var count = 1;
				    if ((data.cursos.length)>0){
				    	var obj = $(".relbox").clone();
				    	$(".inbox-rel").empty();
						$.each(data.cursos, function (index, value) {						
							if (value.type==1)
								var hml = '<div class="rel relbox"><h2>'+count+' - Academia do Pintor '+value.dia+'</h2><div class="text-center"><input type="button" class="editar" data="'+value.id+'" value="Visualizar"> <input type="button" class="editar" data="'+value.id+'" value="Editar"></div></div>'
							else	  	
								var hml = '<div class="rel relbox"><h2>'+count+' - Palestra Tecnica '+value.dia+'</h2><div class="text-center"><input type="button" class="editar" data="'+value.id+'" value="Visualizar"> <input type="button" class="editar" data="'+value.id+'" value="Editar"></div></div>'
							count++;
							$(".inbox-rel").append(hml);		
						});
					}
			    },
			    error: function(){
			    }
			});	

		}

		 var auth = 0;
		 @if (Auth::check())
		 	auth = 1;
		 	getCursos();
		 @endif
		 $("body").on("click",".btnarea",function(e){
		 	e.preventDefault();
		 	if (auth){
		 		$("#form-menu").modal("show");    
		 	}else{
		 		$("#login-area").modal("show");    
		 	}
		 });	
		$("body").on("click",".btnregister-area",function(e){
		 	e.preventDefault();
		 	$("#login-area").modal("hide");    
		 	$("#cadastre-area").modal("show");    
		 });

		 var objPesquisa = {};
		$("body").on("click",".entrarpesquisa",function(e){
		 	e.preventDefault();
		 	var obj = $(this);
		 	obj.val("Carregando...");
		 	$(".emailpesquisa").removeClass("erroinput");
			$.ajax({
			    headers: {
			        'X-CSRF-Token': $('meta[name="_token"]').val()
			    },
			    type: 'GET',
			    url: "{{ URL::route("login.pesquisa") }}",
			    enctype: 'multipart/form-data',
			    data: {"email":$(".emailpesquisa").val()},
			    success: function(data){
			    	if (data.user!=null){
				    	objPesquisa = data.user;
				    	$(".name_pesquisa").val(data.user.name);
				    	$(".email_pesquisa").val(data.user.email);
				    	$(".celphone_pesquisa").val(data.user.celphone);
				    	$(".profissao_pesquisa").val(data.user.profissao);
				    	$(".telefone_pesquisa").val(data.user.telefone);
				    	$(".cep_pesquisa").val(data.user.cep);
				    	$(".address_pesquisa").val(data.user.address);
				    	$(".number_pesquisa").val(data.user.number);
				    	$(".complemento_pesquisa").val(data.user.complemento);
				    	$(".bairro_pesquisa").val(data.user.bairro);
				    	$(".cidade_pesquisa").val(data.user.cidade);
				    	$(".uf_pesquisa").val(data.user.uf);
				    	$(".birth_pesquisa").val(data.user.birth);
						$("#pesquisa-first").modal("hide");    
			 			$("#form-pesquisa").modal("show");    
		 			}else{
						$(".emailpesquisa").addClass("erroinput");
				    	obj.val("Enviar");
		 			}
			    },
			    error: function(){
			   		 $(".emailpesquisa").addClass("erroinput");
			    	obj.val("Enviar");
			    }
			});			 	

		 });

		$("body").on("click",".enviapesquisaform",function(e){
		 	e.preventDefault();
		 	$(this).val("Carregando...");
 			var form = jQuery(this).parents("form:first");
		    var dataString = form.serialize();
		    var formAction = form.attr('action'); 
		 	form.submit();		 	
		 });

		 //LOGIN
		 $("body").on("click",".loginareabtn",function(e){
			$(".erroinput").removeClass("erroinput");
		 	e.preventDefault();
			 var $btn = $(this);
			 $btn.val("carregando...");		
			 $.ajax({
			    headers: {
			        'X-CSRF-Token': $('meta[name="_token"]').val()
			    },
			    type: 'GET',
			    url: "{{ URL::route("login.user") }}",
			    data: {"email":$(".emailloginuser").val(),"password":$(".passwordloginuser").val()},
			    enctype: 'multipart/form-data',
			    success: function(data){
				     if (data.success==1){
				       $("#login-area").modal("hide");   
				       $("#form-menu").modal("show");
				       auth = 1;
				       $(".nomeLabel").html(data.name);
				   	 }else{
				   	 	 $btn.val("Enviar");
				   	 	 $(".emailloginuser").addClass("erroinput");
					 }
			    },
			    error: function(){
			    	 $(".emailloginuser").addClass("erroinput");
			         $btn.val("Enviar");
			    }
			});		  
		});

		$("body").on("click",".editar",function(e){
			e.preventDefault();
			var cod = $(this).attr("data");
			$(this).val("Carregando...");
			var btn = $(this);
			$.ajax({
			    headers: {
			        'X-CSRF-Token': $('meta[name="_token"]').val()
			    },
			    type: 'POST',
			    url: "{{ URL::route("getCursoID") }}",
			    data: {id:cod},
			    enctype: 'multipart/form-data',
			    success: function(data){
			    		btn.val("Visualizar");
			    		$(".inbox").hide();
						$(".inbox-rel").hide();
						var formid = "";
						if (data.obj.type=="1"){
							$(".cursoform").show();							
							formid = $(".cursoform");
						}else{
							$(".palestraform").show();
							formid = $(".palestraform");
						}
						formid.find("input[name=parceiro]").val(data.obj.parceiro);
						formid.find("input[name=cidadecurso]").val(data.obj.cidade);
						formid.find("input[name=unidade]").val(data.obj.unidade);
						var dias = data.obj.dia.split("/")
						formid.find("input[name=dia]").val(dias[0]);
						formid.find("input[name=mes]").val(dias[1]);
						formid.find("input[name=ano]").val(dias[2]);
						formid.find("input[name=editid]").val(data.obj.id);	
						if (data.obj.foto!="")					
							formid.find(".img").html("<img style='width:100px;' src='images/"+data.obj.foto+"'/>");						

						$.each(data.obj.pintores, function (index, value) {
							if (index==0){
								formid.find('.formpintorcurso:last').show();
							}else{
								formid.find('.formpintorcurso:last').clone().find("input:text").val("").end().appendTo('#addpintordiv');
							}
							formid.find('.formpintorcurso:last').find(".nome_pintor").val(value.name);	
							formid.find('.formpintorcurso:last').find(".cep_pintor").val(value.cep);	
							formid.find('.formpintorcurso:last').find(".address_pintor").val(value.address);	
							formid.find('.formpintorcurso:last').find(".number_pintor").val(value.number);	
							formid.find('.formpintorcurso:last').find(".complemento_pintor").val(value.complemento);	
							formid.find('.formpintorcurso:last').find(".bairro_pintor").val(value.bairro);	
							formid.find('.formpintorcurso:last').find(".cidade_pintor").val(value.cidade);	
							formid.find('.formpintorcurso:last').find(".uf_pintor").val(value.uf);	
							formid.find('.formpintorcurso:last').find(".birth_pintor").val(value.birth);	
							formid.find('.formpintorcurso:last').find(".celphone_pintor").val(value.celphone);	
							formid.find('.formpintorcurso:last').find(".telefone_pintor").val(value.telefone);	
							formid.find('.formpintorcurso:last').find(".email_pintor").val(value.email);	
							formid.find('.formpintorcurso:last').find(".profissao_pintor").val(value.profissao);	
							
						});
			    }
			 });
		});

		
		$("body").on("click",".btnadd",function(e){
			e.preventDefault();
			var type = $(this).attr("data");
			if (countform==0){
				$(".formtype"+type).show();
			}else{
				$(".formtype"+type+":last").clone().find("input:text").val("").end().appendTo('#addpintordiv'+type);
			}
			countform++;
		});

		$("body").on("click",".submitcurso",function(e){
			e.preventDefault();
 			var form = jQuery(this).parents("form:first");
		    var dataString = form.serialize();
		    var formAction = form.attr('action'); 
		    form.submit();
		});

		@if (session()->has('flash_notification.message'))
			$("#sucesso-modal").modal("show");
		@endif


		$("body").on("click",".cursobtn",function(e){
			e.preventDefault();
			$(".inbox").hide();
			$(".inbox-rel").hide();
			$(this).next(".inbox").slideDown();
		});


		$("body").on("click",".palestrabtn",function(e){
			e.preventDefault();
			$(".inbox").hide();
			$(".inbox-rel").hide();
			$(this).next(".inbox").slideDown();
		});


		$("body").on("click",".relbtn",function(e){
			e.preventDefault();
			$(".inbox").hide();
			$(".inbox-rel").hide();
			$(this).next(".inbox-rel").slideDown();
		});		

		$("body").on("click",".btnregisterform-area",function(e){
			$(".erroinput").removeClass("erroinput");
		 	e.preventDefault();
			 var $btn = $(this);
			 $btn.val("carregando...");
		    e.preventDefault();
		    var valida = true;
		   	if ($("input[name=email]").val()==""){
		   		valida = false;
		   		$("input[name=email]").addClass("erroinput");
		    }
		   	if ($("input[name=name]").val()==""){
		   		valida = false;
		   		$("input[name=name]").addClass("erroinput");
		    }
		   	if ($("input[name=rede]").val()==""){
		   		valida = false;
		   		$("input[name=rede]").addClass("erroinput");
		    }
		   	if ($("input[name=telefone]").val()==""){
		   		valida = false;
		   		$("input[name=telefone]").addClass("erroinput");
		    }	
		   	if ($("input[name=ddd]").val()==""){
		   		valida = false;
		   		$("input[name=ddd]").addClass("erroinput");
		    }			    	

		    if (!valida){
		    	return false;
			}    		    		    

		    var form = jQuery(this).parents("form:first");
		    var dataString = form.serialize();
		    var formAction = form.attr('action');   
			$.ajax({
			    headers: {
			        'X-CSRF-Token': $('meta[name="_token"]').val()
			    },
			    type: 'GET',
			    url: "{{ URL::route("register.user") }}",
			    data: dataString,
			    enctype: 'multipart/form-data',
			    success: function(data){
				     if (data.success==1){
				       $("#cadastre-area").modal("hide");   
				       $("#sucesso-modal").modal("show");
				       auth = 1;
				       $(".nomeLabel").html($("input[name=name]").val());
				   	 }else{
				   	 	 $btn.val("Enviar");
				   	 	 $("input[name=email]").addClass("erroinput");
					 }
			    },
			    error: function(){
			    	 $("input[name=email]").addClass("erroinput");
			         $btn.val("Enviar");
			    }
			});

		 });

		$("body").on("click",".btnpesquisa",function(e){
		 	e.preventDefault();  
		 	$("#pesquisa-first").modal("show");    
		 });

		$("body").on("click",".btnregisterpesquisa",function(e){
		 	e.preventDefault();  
		 	$("#pesquisa-first").modal("hide");   
		 	$("#cadastre-pesquisa").modal("show");    
		 });		 
		 

		 
    })
@endsection

