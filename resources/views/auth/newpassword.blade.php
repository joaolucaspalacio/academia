@extends("layout.master")

@section("content")
  <div class="container-full">
    <div class="registerblue-header text-center">
      <img src="images/registerblue.png"/>
    </div>
    <div class="conteudo">
      <div class="row">
        <div class="text-center col-md-4 col-md-offset-4 registerstep col-sm-8 col-sm-offset-2">
          <h3>Digite sua nova senha:</h3>
          @if ( session()->has('error') )
              <div class="alert alert-danger alert-dismissable">{{ session()->get('error') }}</div>
          @endif
                 
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
            {!! csrf_field() !!}

            <p><input type="password" id="password"  name="password" placeholder="Senha"/></p>
            <p><input type="password" id="confirm"   name="confirm"  placeholder="Confirmar senha"/></p>

        
            <p>
                <input type="button" class="registerbtn" value="Concluir nova senha"/>
            </p>
          </form>
        </div> 
      </div>
    </div>
  </div>

@endsection

@section("scripts")

  $("p").on("click",".registerbtn",function(e){
    $(".error").removeClass("error");
    e.preventDefault();
    var error = false;
    var password = document.getElementById('password');
    var confirm = document.getElementById('confirm');
    if(confirm.value!=password.value || password.value==""){
      $("input[name=password]").addClass("error");
      $("input[name=confirm]").addClass("error");
      error = true;
    }     

    if (error){
     return false;
    }
    $(".register-form").submit();
  });

  
 


@endsection

