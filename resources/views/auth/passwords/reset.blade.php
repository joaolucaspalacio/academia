@extends("layout.master")

@section("content")
  <div class="container-full">
    <div class="registerblue-header text-center">
      <img src="{{url("images/registerblue.png")}}"/>
    </div>
    <div class="conteudo">
      <div class="row">
        <div class="text-center col-md-4 col-md-offset-4 registerstep col-sm-8 col-sm-offset-2">
          <h3>Digite sua nova senha:</h3>
          @if ( session()->has('errortoken') )
              <div class="alert alert-danger alert-dismissable">{{ session()->get('errortoken') }}</div>
          @endif                    

            <form class="register-form" role="form" method="POST" action="{{ url('/password/newpassword') }}">
                        {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <p><input type="text" id="email" value="{{ $email or old('email') }}" name="email" placeholder="Email" /></p>
            <p><input type="password" id="password"  name="password" placeholder="Senha"/></p>
            <p><input type="password" id="password_confirmation"   name="password_confirmation"  placeholder="Confirmar senha"/></p>

        
            <p>
                <input type="button" class="registerbtn" value="Salvar nova senha"/>
            </p>
          </form>
        </div> 
      </div>
    </div>
  </div>

@endsection

@section("scripts")

  $("p").on("click",".registerbtn",function(e){
    $(".error").removeClass("error");
    e.preventDefault();
    var error = false;
    var password = document.getElementById('password');
    var confirm = document.getElementById('password_confirmation');
    if(confirm.value!=password.value || password.value==""){
      $("input[name=password]").addClass("error");
      $("input[name=password_confirmation]").addClass("error");
      error = true;
    }     

    if (error){
     return false;
    }
    $(".register-form").submit();
  });

  
 


@endsection

<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>-->

