<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
/* -------------------------------------
    GLOBAL
------------------------------------- */
* {
  font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
  font-size: 100%;
  line-height: 1.6em;
  margin: 0;
  padding: 0;
}
img {
  max-width: 600px;
  width: auto;
}
body {
  -webkit-font-smoothing: antialiased;
  height: 100%;
  -webkit-text-size-adjust: none;
  width: 100% !important;
}
/* -------------------------------------
    ELEMENTS
------------------------------------- */
a {
}
.btn-primary {
  margin-bottom: 10px;
  width: auto !important;
}
.btn-primary td {
  background-color: #348eda; 
  border-radius: 25px;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
  font-size: 14px; 
  text-align: center;
  vertical-align: top; 
}
.btn-primary td a {
  background-color: #348eda;
  border: solid 1px #348eda;
  border-radius: 25px;
  border-width: 10px 20px;
  display: inline-block;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  line-height: 2;
  text-decoration: none;
}
.last {
  margin-bottom: 0;
}
.first {
  margin-top: 0;
}
.padding {
  padding: 10px 0;
}
/* -------------------------------------
    BODY
------------------------------------- */
table.body-wrap {
  padding: 20px;
  width: 100%;
}
table.body-wrap .container {
  border: 1px solid #f0f0f0;
}
/* -------------------------------------
    FOOTER
------------------------------------- */
table.footer-wrap {
  clear: both !important;
  width: 100%;  
}
.footer-wrap .container p {
  color: #666666;
  font-size: 12px;
  
}
table.footer-wrap a {
  color: #999999;
}
/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, 
h2, 
h3 {
  color: #111111;
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 200;
  line-height: 1.2em;
  margin: 40px 0 10px;
}
h1 {
  font-size: 36px;
}
h2 {
  font-size: 28px;
}
h3 {
  font-size: 22px;
}
p, 
ul, 
ol {
  font-size: 14px;
  font-weight: normal;
  margin-bottom: 10px;
}
ul li, 
ol li {
  margin-left: 5px;
  list-style-position: inside;
}
/* ---------------------------------------------------
    RESPONSIVENESS
------------------------------------------------------ */
/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  clear: both !important;
  display: block !important;
  Margin: 0 auto !important;
  max-width: 600px !important;
}
/* Set the padding on the td rather than the div for Outlook compatibility */
.body-wrap .container {

}
/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  display: block;
  margin: 0 auto;
  max-width: 600px;
}
/* Let's make sure tables in the content area are 100% wide */
.content table {
  width: 100%;
}
a:active, a :visited{
    color: white;
}
            .logo{background-color: white;text-align: center;padding: 20px;}
            .part1{background-color: #f3b035;text-align: center; padding: 40px;}
            .part2{background-color: #009fe2;text-align: center;padding: 40px;}
            .nao{
                color:#1b0702;
                font-size: 12px;
            }
            .link{
                font-size: 16px;color: white;
            }
        </style>
    </head>
        <body bgcolor="#e4e4e4">

        <!-- body -->
        <table class="body-wrap" bgcolor="#f6f6f6">
          <tr>
            <td></td>
            <td class="container" bgcolor="#FFFFFF">

              <!-- content -->
              <div class="content">
              <table>
                <tr>
                  <td>
                        <div class="logo" style="background-color: white;text-align: center;padding: 20px;"><img src="{{url("/")}}/images/logoemail.png"/></div>
                        <div class="part1" style="background-color: #f3b035;text-align: center; padding: 40px;"><img src="{{url("/")}}/images/emailpart1.png"/></div>
                        <div class="part2" style="background-color: #009fe2;text-align: center;padding: 40px;">
                        <p><img src="{{url("/")}}/images/deubranco.png"/></p>
                        <p class="nao" style=" color:#1b0702;              font-size: 12px;">Não tem problema. Com tanto chocolate  assim, fica difícil prestar atenção na senha.</p>
                        <p class="link" style="font-size: 16px;color: white;"><a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>para criar uma nova!</p>
                        <p class="nao" style=" color:#1b0702;              font-size: 12px;">Agora você já pode acessar o site e tentar deliciosas mordidas.</p>
                        <p class="nao" style=" color:#1b0702;              font-size: 12px;">#comaosite</p>
                        </div>
                    </td>
                </tr>
            </table>
            

       
    </body>
</html>