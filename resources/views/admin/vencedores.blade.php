{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Lista de Vencedores')

@section('content_header')
    <h1>Vencedores</h1>
@stop

@section('content')
    

        <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-header with-border">
                    <a href="{{route("admin.excel")}}">Gerar Excel</a>
                    
                  </div>
                  <div class="box-body table-responsive">
                    <div class="form-group">
                      <label>Selecione o Dia</label>
                      <select class="form-control" id="selectdays">
                        @if (isset($dias))
                        @foreach($dias as $value)
                          <option @if($selecionado==$value->liberado) selected @endif value="{{str_replace("/","-",$value->liberado)}}">{{$value->liberado}}</option>
                        @endforeach
                        @endif
                      </select>
                    </div>
                      <table class="table table-condensed table-hover" id="dataTable">
                          <thead>
                            <tr>
                              <td>Nome</td>
                              <td>Email</td>
                              <td>CPF</td>
                              <td>Aceita Email</td>
                              <td>Tipo</td>
                              <td>Chocolate</td>
                              <td>Liberado em</td>                              
                              <td>Clicado</td>
                            </tr>
                          </thead>
                          <tbody>
                            @if (isset($prizes))
                              @foreach ($prizes as $column)
                                <tr>
                                  <td>{{$column->user->name}}</td>
                                  <td>{{$column->user->email}}</td>
                                  <td>{{$column->user->cpf}}</td>
                                  <td>{{$column->opt_in}}</td>
                                  <td>{{$column->prize->type=="cash" ? "Dinheiro + chocolate ": "Chocolate"}}</td>
                                  <td>{{$column->prize->chocolate->title}} + {{$column->prize->chocolate->type}}</td>
                                  <td>{{$column->prize->liberado->format("d/m H:i:s")}}</td>
                                  <td>{{$column->updated_at->format("d/m H:i:s")}}</td>
     
                                </tr>
                              @endforeach    
                            @endif                        
                          </tbody>                          
                          <tfoot>
                          
                          </tfoot>
                      </table>
                  </div>
              </div>
          </div>
      </div>
@stop

@section('css')
  <meta name="_token" content="{!! csrf_token() !!}" />
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/datepicker/datepicker3.css')}}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">    
@stop

@section('js')
  <!-- bootstrap datepicker -->
    <script src="{{url('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });    
    $("body").on("blur",".order",function(e){
      var order = $(this).val();
      var obj = $(this);
      $(this).val("Atualizado...");
      $.ajax({
        url: "{{route('update.order')}}",
        data: {order:order,id:$(this).attr("data")},
        method:"post"
      }).done(function() {
        obj.val(order);
      });
    });
    $( "#selectdays" )
      .change(function () {
        var str = "";
        var url = '{{ route("admin.vencedores.date", ":id") }}';
        url = url.replace(':id', $( "#selectdays option:selected" ).val() );
         window.location.href  =   url;
      });      
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
    });
    </script>
@stop