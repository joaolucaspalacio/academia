{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Pesquisas</h1>
@stop

@section('content')
    <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-header with-border">
                  </div>
                  <div class="box-body table-responsive">
                      <table class="table table-condensed table-hover" id="dataTable">
                          <thead>
                          	<tr>
                          		<td>Nome</td>
                              <td>Curso</td>
                              <td>Conteudo</td>
                          		<td>Conhecimento</td>
                          		<td>Programa</td>
                          		<td>Material</td>
                              <td>REgular ou Negativo</td>
                              <td>características</td>
                              <td>Produtos</td>
                              <td>Opiniao</td>
                          	</tr>
                          </thead>
                          <tbody>
                          	@if (isset($model))
                              @foreach ($model as $column)
                                <tr>
                                  <td>{{$column->pintor->name or ""}}</td>
                                  <td>{{$column->cursos->type or ""}}</td>
                                  <td>{{$column->conteudo}}</td>
                                  <td>{{$column->conhecimento}}</td>
                                  <td>{{$column->programa}}</td>
                                  <td>{{$column->material}}</td>
                                  <td>{{$column->motivos}}</td>
                                  <td>{{$column->caract1}}, {{$column->caract2}}, {{$column->caract3}}</td>
                                  <td>{{$column->alta1}}, {{$column->alta2}}</td>
                                  <td>{{$column->opiniao}}</td>
                                </tr>
                              @endforeach    
                            @endif                        
                          </tbody>                          
                          <tfoot>
                          
                          </tfoot>
                      </table>
                  </div>
              </div>
          </div>
      </div>
@stop

@section('css')
 <meta name="_token" content="{!! csrf_token() !!}" />
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
  <!-- bootstrap datepicker -->
    <script src="{{url('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>


@stop