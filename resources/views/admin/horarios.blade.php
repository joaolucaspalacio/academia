{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Horarios dos premios')

@section('content_header')
    <h1>Cadastro de Horarios</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
			<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Horarios dos premios</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" enctype='multipart/form-data' action="{{route("admin.gravar.horarios")}}">
              {!! csrf_field() !!}	
              <div class="box-body">
                @if (isset($success))
                    <div class="alert alert-success">
                        <ul>
                           <li>Gravado com sucesso!</li>
                        </ul>
                    </div>
                @endif   

	              @if (count($errors) > 0)
	                  <div class="alert alert-danger">
	                      <ul>
	                          @foreach ($errors->all() as $error)
	                              <li>{{ $error }}</li>
	                          @endforeach
	                      </ul>
	                  </div>
	              @endif              	
                <div class="form-group">
                  <label>Data:</label>

                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="data"  class="form-control pull-right" id="datepicker">
                  </div>
                  <!-- /.input group -->
                </div>     
                <div class="form-group">
                  <label>Horario (hh:mm:ss)</label>

                  <div class="input-group">
                    <input type="text" name="time" class="form-control timepicker">

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>                       
                <div class="form-group">
                  <label for="">Premio de:</label>
                  <select  name="type" class="form-control">
                  	<option value="cash">R$ 300,00 + Pedaço de chocolate</option>
                  	<option value="chocolate">Pedaço de chocolate</option>
                  </select>
                </div>                                                               
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>  
    </div>


    <section class="content-header">
        <h1>
            {{{ $title or "" }}}
            @if(isset($subtitle))
                <small>{{{ $subtitle }}}</small>
            @endif
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> {{{ $title or "" }}}</li>
        </ol>
    </section>

    <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-header with-border">
                  </div>
                  <div class="box-body table-responsive">
                      <table class="table table-condensed table-hover" id="dataTable">
                          <thead>
                          
                          </thead>
                          <tbody>
                              @foreach ($prizes as $column)
                                <tr>
                                  <td>{{$column->liberado->format("d/m/Y H:i:s")}}</td>
                                  <td>{{$column->type=="cash" ? "R$ 300,00 + Pedaco de chocolate " : " Pedaco de chocolate"}}</td>
                                  <td><a class="delete" href="{{route('admin.horarios.excluir',$column->id)}}"><span class="label label-danger">excluir</span></a></td>
                                </tr>
                              @endforeach                            
                          </tbody>                          
                          <tfoot>
                          
                          </tfoot>
                      </table>
                  </div>
              </div>
          </div>
      </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/datepicker/datepicker3.css')}}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">    
@stop

@section('js')

    <!-- bootstrap datepicker -->
    <script src="{{url('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    <script>
   //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
    });

    $("body").on("click",".delete",function(e){
        if (!confirm("Deseja deletar esse horario?")){
          e.preventDefault();
          return false;
        }
    });
   //Timepicker
    $(".timepicker").timepicker({
                minuteStep: 1,
                template: 'modal',
                appendWidgetTo: 'body',
                showSeconds: true,
                showMeridian: false,
                defaultTime: false
            });

     </script>
@stop