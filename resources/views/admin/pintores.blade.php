{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Pintores')

@section('content_header')
    <h1>Lista de pintores</h1>
@stop

@section('content')


        <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-header with-border">
                  </div>
                  <div class="box-body table-responsive">

                      <table class="table table-condensed table-hover" id="dataTable">
                          <thead>
                            <tr>
                              <td>Nome</td>
                              <td>Telefone</td>
                              <td>Rede</td>
                              <td>Email</td>                                                            
                            </tr>
                          </thead>
                          <tbody>
                            @if (isset($model))
                              @foreach ($model as $column)
                                <tr>
                                  <td>{{$column->name}}</td>
                                  <td>{{$column->telefone}}</td>
                                  <td>
                                     {{$column->rede}}
                                  </td>                                  
                                  <td>{{$column->email}}</td>
                                </tr>
                              @endforeach    
                            @endif                        
                          </tbody>                          
                          <tfoot>
                          
                          </tfoot>
                      </table>
                  </div>
              </div>
          </div>
      </div>
@stop

@section('css')
  <meta name="_token" content="{!! csrf_token() !!}" />
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/datepicker/datepicker3.css')}}">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.css')}}">    
@stop

@section('js')
  <!-- bootstrap datepicker -->
    <script src="{{url('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- bootstrap time picker -->
    <script src="{{url('bower_components/AdminLTE/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>

    
@stop