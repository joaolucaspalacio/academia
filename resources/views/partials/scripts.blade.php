  <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
  <script src="plugins/enscroll-0.6.2.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.1/inputmask/inputmask.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <script src="{{url("plugins/jquery.mask.min.js")}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="js/jquery.customSelect.min.js"></script>

  <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    });


$('#select').customSelect();

  //UF FILTER
  $('#select').on('change', function(e) {
    e.preventDefault();
    //VALUE OF SELECT
    $('div.box').addClass('hide');
    var id = $(this).val();

    if(id){
      $('div.'+id).removeClass('hide');
    }
  
    switch(id){
      case("SP"):
        $('h3').text("São Paulo");
      break;
      case("MG"):
        $('h3').text("Minas Gerais");
      break;
      case("RJ"):
        $('h3').text("Rio de Janeiro");
      break;
      case("DF"):
        $('h3').text("Distrito Federal");
      break;
      case("GO"):
        $('h3').text("Goiás");
      break;
      case("PE"):
        $('h3').text("Pernambuco");
      break;
      case("RN"):
        $('h3').text("Rio Grande do Norte");
      break;
      case("BA"):
        $('h3').text("Bahia");
      break;
      case("CE"):
        $('h3').text("Ceará");
      break;
      case("AL"):
        $('h3').text("Alagoas");
      break;
      case("PB"):
        $('h3').text("Paraíba");
      break;
      case("PR"):
        $('h3').text("Paraná");
      break;
      case("MS"):
        $('h3').text("Mato Grosso do Sul");
      break;
      default:
        $('h3').text("");
    }

    $('html, body').scrollTop($("#select").offset().top);

    //Event para o GA para monitor os estados selecionados
    ga('send','event', 'select', 'change', id);
  });

    $("body").on("click",".openMenu",function(e){
        e.preventDefault();
        $(".mobile-menu").toggle();
        $(".modal-mobile-menu").toggle();
    });
    

  

      /*!
    * Gerador e Validador de CPF v1.0.0
    * https://github.com/tiagoporto/gerador-validador-cpf
    * Copyright (c) 2014-2015 Tiago Porto (http://www.tiagoporto.com)
    * Released under the MIT license
    */
    function CPF(){"user_strict";function r(r){for(var t=null,n=0;9>n;++n)t+=r.toString().charAt(n)*(10-n);var i=t%11;return i=2>i?0:11-i}function t(r){for(var t=null,n=0;10>n;++n)t+=r.toString().charAt(n)*(11-n);var i=t%11;return i=2>i?0:11-i}var n=false,i=true;this.gera=function(){for(var n="",i=0;9>i;++i)n+=Math.floor(9*Math.random())+"";var o=r(n),a=n+"-"+o+t(n+""+o);return a},this.valida=function(o){for(var a=o.replace(/\D/g,""),u=a.substring(0,9),f=a.substring(9,11),v=0;10>v;v++)if(""+u+f==""+v+v+v+v+v+v+v+v+v+v+v)return n;var c=r(u),e=t(u+""+c);return f.toString()===c.toString()+e.toString()?i:n}}

    function calcAge(dateString) {
      var birthday = +new Date(dateString);
      return ~~((Date.now() - birthday) / (31557600000));
    }

     var CPF = new CPF();
     $('.date').mask('00/00/0000');
      $('.cnpj').mask('00.000.000/0000-00', {reverse: true});

      $('.birth_pintor').mask('00/00/0000');
      $('input[name=cep]').mask('00000-000');
      $('input[name=dia]').mask('00');
      $('input[name=mes]').mask('00');
      $('input[name=ano]').mask('0000');
      $('input[name=celphone]').mask('(00)00000-0000');
      

  	function resizeNav(){
    	if ($(window).width()>992){
    		$(".itens").css({"marginTop":($(window).height()+5)+"px"});	
    	}else{
    		$(".itens").css({"marginTop":($(window).height())+"px"});	
    	}  		
  	}

  	$(function(){
      Inputmask().mask(document.querySelectorAll("input"));
  		resizeNav();    

      $body = $("body");


      

      @yield("scripts")





  	});
    
    $(window).on("resize",function(e){
    	resizeNav();    
    })
  </script>
  
   