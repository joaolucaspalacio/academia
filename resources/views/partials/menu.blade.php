		<header id="header">
			<h1 id="logo">
				<span>Sherwin-Willians - Academia do Pintor</span>
			</h1>
			<h2><a href="#" class="btnarea">Área Restrita</a>   |   <a href="#" class="btnpesquisa">Pesquisa</a> </h2>
			<p class="pull-left">É nisso que nós, da Sherwin-Williams, acreditamos.<br><br>Por este motivo, junto com o apoio e parceria de empresas que investem na capacitação profissional, criamos a <span>Academia do pintor da Sherwin-Williams</span>, um programa desenvolvido com o objetivo de formar, aperfeiçoar e profissionalizar pintores e trabalhadores da construção civil que estão iniciando na atividade de pintura.<br><br><span class="destak">Não deixe sua profissão passar em branco.<br><!--Inscreva-se agora!--></span></p>
			<p class="pull-right">A Sherwin-Williams tem o maior orgulho de ser a primeira fabricante de tintas a apoiar o <b>Movimento Brasil por um Pintor Melhor</b>.</p>
		</header>