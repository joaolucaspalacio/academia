
<!-- Modal HTML -->
<div id="login-area" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<h1>Área Restrita</h1>
            	<p><input type="text" class="emailloginuser" placeholder="Login"/></p>
            	<p><input type="password" class="passwordloginuser" placeholder="Senha"/></p>
            	<div class="col-md-12"><p class="pull-right"><input type="button" class="loginareabtn" value="Entrar"/></p></div>
            	<p class="sem"><b>Primeiro acesso?</b></p>
				<p><a href="#" class="btnregister-area">Clique aqui</a> e cadastre-se.</p>
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="sucesso-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding:60px 80px">
            	<h1 class="text-center">Dados enviados com <b>sucesso</b>.</h1>
            </div>
        </div>
    </div>
</div>


<!-- Modal HTML -->
<div id="cadastre-area" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<h1>Cadastre-se</h1>
            	<form class="registerarea-form">
            	<p><input type="text" name="name" placeholder="Nome"/></p>
            	<p><input type="text"  name="rede" placeholder="Rede"/></p>
            	<div class="col-md-12" style="padding:0px">
            		<p class="col-md-2 semp"><input  name="ddd" type="text" placeholder="DDD"/></p>
            		<p class="col-md-10 semp"><input  name="telefone" type="text" placeholder="Telefone"/></p>
            	</div>
            	<p><input type="text"  name="email" placeholder="E-mail"/></p>
            	<div class="col-md-12"><p class="pull-right"><input class="btnregisterform-area" type="button" value="Entrar"/></p></div>
            	<br clear="all"/>
            	</form>
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="cadastre-pesquisa" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<h1>Cadastre-se</h1>
            	<p><input type="text" placeholder="Nome"/></p>
            	<div class="col-md-12" style="padding:0px">
            		<p class="col-md-2 semp"><input type="text" placeholder="DDD"/></p>
            		<p class="col-md-10 semp"><input type="text" placeholder="Telefone"/></p>
            	</div>
            	<p><input type="text" placeholder="E-mail"/></p>
            	<div class="col-md-12"><p class="pull-right"><input type="button" value="Entrar"/></p></div>
            	<br clear="all"/>
            </div>
        </div>
    </div>
</div>


<!-- Modal HTML -->
<div id="sucesso-cadastro" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<h1>Cadastro efetuado com sucesso.</h1>
            	<p><input type="text" placeholder="Login"/></p>
            	<p><input type="text" placeholder="Senha"/></p>
            	<div class="col-md-12"><p class="pull-right"><input type="button" value="Entrar"/></p></div>
            	<br clear="all"/>
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="pesquisa-first" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            	<h1>PESQUISA</h1>
            	<p><input type="text" class="emailpesquisa" placeholder="E-mail"/></p>
            	<div class="col-md-12 sem semp">
            		<div class="col-md-10 sem semp">
            			<p class="collapse error">
            			E-mail não cadastrado. </br/>
						<!--Cadastre-se para seu primeiro acesso.-->
						</p>
            		</div>
            		<div class="col-md-2 text-right">
            			<input type="button" class="entrarpesquisa" value="Entrar"/></p>
            		</div>
            	</div>
            	<p class="sem ">&nbsp</p>
				
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="form-menu" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body dark">
            	<h1 class="text-center"><b>Olá</b>, 
            		@if (Auth::check())
            		<span class="nomeLabel">{{Auth::user()->name}}</span>
            		@else
            		<span class="nomeLabel">Fulano</span>
            		@endif
            	</h1>
            	<p class="text-center">Escolha abaixo o que deseja cadastrar</p>
            	<div class="box cursobtn">
            		<p class="title"><b>aCADEMIA</b> DO PINTOR</p>
            		<p class="subtitle">Cursos extensos</p>
            	</div>
            	<div class="inbox collapse cursoform">
            		@include("partials.form",["type"=>1])
	            	<br clear="all"/>	            	
            	</div>
            	<div class="box palestrabtn">
            		<p class="title"><b>Palestra</b> técnica</p>
            		<p class="subtitle">Conteúdos de até 3 horas </p>
            	</div>
            	<div class="inbox collapse palestraform">
            		@include("partials.form",["type"=>2])  
            		<br clear="all"/>	          	
            	</div>            	
            	<div class="box relbtn">
            		<p class="title"><b>Relatório</b> de Cadastros</p>
            	</div>     
            	<div class="inbox-rel collapse">

          		
            	</div>       	            	
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="form-pesquisa" class="modal fade">
    <div class="modal-dialog portrait">
        <div class="modal-content">
            <div class="modal-body dark">
            	<div class="box">
            		<p class="title"><b>pesquisa</b> Sherwin-Williams </p>
            	</div>
                <form action="{{route("send.pesquisa")}}" method="POST">
                    <div class="scroll">
                    	<div class="col-md-10 col-md-offset-1">
        	            	<div class="col-md-12" style="padding:0px">
        	            		<div class="">
        	            			<p class="col-md-12 semp"><input type="text" name="name" class="name_pesquisa" placeholder="Nome Completo"/></p>
        	            			<p class="col-md-8 semp"><input type="text" name="cep" class="cep_pesquisa" placeholder="CEP 00000-000"/></p>
        	            			<p class="col-md-10 semp"><input type="text"  name="address"class="address_pesquisa"placeholder="Endereço"/></p>
        	            			<p class="col-md-2 semp"><input type="text" name="number" class="number_pesquisa"placeholder="Nº"/></p>
        	            			<p class="col-md-6 semp"><input type="text"  name="complemento"class="complemento_pesquisa"placeholder="Complemento"/></p>
        	            			<p class="col-md-6 semp"><input type="text"  name="bairro"class="bairro_pesquisa"placeholder="Bairro"/></p>
        	            			<p class="col-md-10 semp"><input type="text" name="cidade" class="cidade_pesquisa" placeholder="Cidade"/></p>
        	            			<p class="col-md-2 semp"><input type="text"  name="uf"class="uf_pesquisa"placeholder="UF"/></p>
        	            			<p class="col-md-12 semp"><input type="text" name="birth"class="birth_pesquisa" placeholder="Data de Nascimento 00/00/0000"/></p>
        	            			<p class="col-md-12 semp"><input type="text" name="celphone" class="celphone_pesquisa"placeholder="Celular"/></p>
        	            			<p class="col-md-12 semp"><input type="text" name="telefone" class="telefone_pesquisa"placeholder="Telefone Fixo"/></p>
        	            			<p class="col-md-12 semp"><input type="text"  name="email"class="email_pesquisa"placeholder="E-mail"/></p>
        	            			<p class="col-md-12 semp"><input type="text"  name="profissao"class="profissao_pesquisa"placeholder="Profissão"/></p>	 
        	            			<br clear="all"/>            			
        	            		</div>
        	            	</div>    
        	            	<br clear="all"/>  
        	            	<p class="col-md-12 ">
        	            		Seus comentários são importantes para nós. <br/>
        						Por favor, complete a pesquisa de Satisfação abaixo:
        	            	</p>
        	            	<div class="modaltable">
        	            		<table>
        	            			<thead>
        	            				<tr>
        	            					<td></td>
        	            					<td>EXCELENTE</td>
        	            					<td>MUITO BOM</td>
        	            					<td>BOM</td>
        	            					<td>REGULAR</td>
        	            					<td>RUIM</td>
        	            				</tr>
        	            			</thead>
        	            			<tbody>
        	            				<tr>
        	            					<td class="text-right">Conteúdo do curso</td>
        	            					<td class="text-center"><input name="conteudo" value="5" checked type="radio"/></td>
        	            					<td class="text-center"><input name="conteudo" value="4" type="radio"/></td>
        	            					<td class="text-center"><input  name="conteudo" value="3"type="radio"/></td>
        	            					<td class="text-center"><input  name="conteudo" value="2"type="radio"/></td>
        	            					<td class="text-center"><input name="conteudo" value="1" type="radio"/></td>
        	            				</tr>	  
        	            				<tr>
        	            					<td class="text-right">Conhecimento dos palestrantes</td>
        	            					<td class="text-center"><input  name="conhecimento" value="5" checked type="radio"/></td>
        	            					<td class="text-center"><input name="conhecimento" value="4"  type="radio"/></td>
        	            					<td class="text-center"><input  name="conhecimento" value="3" type="radio"/></td>
        	            					<td class="text-center"><input  name="conhecimento" value="2" type="radio"/></td>
        	            					<td class="text-center"><input name="conhecimento" value="1"  type="radio"/></td>
        	            				</tr>	  
        	            				<tr>
        	            					<td class="text-right">Programa do curso</td>
        	            					<td class="text-center"><input name="programa" value="5"  checked type="radio"/></td>
        	            					<td class="text-center"><input name="programa" value="4"  type="radio"/></td>
        	            					<td class="text-center"><input name="programa" value="3"  type="radio"/></td>
        	            					<td class="text-center"><input name="programa" value="2"  type="radio"/></td>
        	            					<td class="text-center"><input name="programa" value="1"  type="radio"/></td>
        	            				</tr>	  
        	            				<tr>
        	            					<td class="text-right">Material didático do curso</td>
        	            					<td class="text-center"><input name="material" value="5" checked type="radio"/></td>
        	            					<td class="text-center"><input name="material" value="4"  type="radio"/></td>
        	            					<td class="text-center"><input name="material" value="3"  type="radio"/></td>
        	            					<td class="text-center"><input name="material" value="2"  type="radio"/></td>
        	            					<td class="text-center"><input name="material" value="1"  type="radio"/></td>
        	            				</tr>	  	            					            					            				  
        	            					            					            					            					            				        				
        	            			</tbody>
        	            		</table>
        	            	</div>	                      
                    	</div>   
                    	<br clear="all"/>	            	
                    	<div class="col-md-10 col-md-offset-1">
        	            	<p class="col-md-12">Caso tenha existido algum ponto regular ou ruim, por favor, indique os motivos: </p>
        	            	<p class="col-md-12 text-center"><textarea name="motivos"></textarea></p>
        	            	<p class="col-md-12">Nomeie 3 características que uma pintura deve ter, na sua opinião, para ser bem acabada:</p>
        	            	<p class="col-md-6 col-md-offset-3"><input name="caract1" type="text"/></p>
        	            	<p class="col-md-6 col-md-offset-3"><input name="caract2" type="text"/></p>
        	            	<p class="col-md-6 col-md-offset-3"><input name="caract3" type="text"/></p>
        	            	<p class="col-md-12" >Nomeie 2 produtos que considere de alta qualidade:</p>
        	            	<p class="col-md-6 col-md-offset-3"><input  name="alta1" type="text"/></p>
        	            	<p class="col-md-6 col-md-offset-3"><input  name="alta2" type="text"/></p>
        	            	<p class="col-md-12">Qual a sua opinião sobre o Curso Prático de Pintura da Academia do Pintor Sherwin-Williams?</p>
                    		<p class="col-md-4"><input name="opiniao" value="3" type="radio"/> Muito importante</p>
                    		<p class="col-md-4"><input name="opiniao" value="2" type="radio"/> Importante</p>
                    		<p class="col-md-4"><input name="opiniao" value="1" type="radio"/> Não importante</p>
                    	</div>   
                    	<br clear="all"/>	 
                    </div>
                	<div class="col-md-12"><p class="text-center"><input type="button" class="enviapesquisaform" value="Enviar"/></p></div>
                	<br clear="all"/>	             	
                </form>
            </div>
        </div>
    </div>
</div>