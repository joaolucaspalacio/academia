            		<form  method="post"  enctype="multipart/form-data" action="{{route("gravacurso")}}" >
            			{{ csrf_field() }}
	            		<h3>Parceiro</h3>
	            		<input type="hidden" name="tipo" value="{{$type}}"/>
	            		<input type="hidden" name="editid" value=""/>
		            	<div class="col-md-12" style="padding:0px">
		            		<p class="col-md-12 semp"><input type="text" name="parceiro"  placeholder="Nome Parceiro"/></p>
		            		<p class="col-md-12 semp"><input type="text"  name="cidadecurso"  placeholder="Cidade"/></p>
		            		<p class="col-md-12 semp"><input type="text"  name="unidade"  placeholder="Unidade"/></p>
		            		<p class="col-md-4 "><input type="text"  name="dia"  placeholder="Dia" maxlength="2"/></p>
		            		<p class="col-md-4 "><input type="text"  name="mes"  placeholder="Mes" maxlength="2"/></p>
		            		<p class="col-md-4 "><input type="text"  name="ano"  placeholder="Ano" maxlength="4"/></p>
		            	</div>    
		            	<br clear="all"/>      
	            		<h3>Participantes<small> (obrigatório nome e email) </small></h3>
		            	<div class="col-md-12 " id="addpintordiv{{$type}}" style="padding:0px">
		            		<p class="col-md-7 semp num">Numero de Pintores Treinados</p>
		            		<p class="col-md-2 semp"> <input type="text" name="participantes" placeholder="0"/></p>
		            		<div class="formpintorcurso formtype{{$type}} collapse">
		            			<p class="col-md-12 semp"><input type="text" class="painterinput nome_pintor"  name="name[]" value="" placeholder="Nome Completo"/></p>
		            			<p class="col-md-8 semp"><input type="text" class="painterinput cep_pintor"   name="cep[]"  placeholder="CEP 00000-000"/></p>
		            			<p class="col-md-10 semp"><input type="text" class="painterinput address_pintor"   name="address[]"  placeholder="Endereço"/></p>
		            			<p class="col-md-2 semp"><input type="text"  class="painterinput number_pintor"  name="number[]"  placeholder="Nº"/></p>
		            			<p class="col-md-6 semp"><input type="text"  class="painterinput complemento_pintor"  name="complemento[]"  placeholder="Complemento"/></p>
		            			<p class="col-md-6 semp"><input type="text"  class="painterinput bairro_pintor"  name="bairro[]"  placeholder="Bairro"/></p>
		            			<p class="col-md-10 semp"><input type="text"  class="painterinput cidade_pintor"  name="cidade[]"  placeholder="Cidade"/></p>
		            			<p class="col-md-2 semp"><input type="text"  class="painterinput uf_pintor"  name="uf[]"  placeholder="UF"/></p>
		            			<p class="col-md-12 semp"><input type="text" class="painterinput birth_pintor"   name="birth[]"  placeholder="Data de Nascimento 00/00/0000"/></p>
		            			<p class="col-md-12 semp"><input type="text"  class="painterinput celphone_pintor"  name="celphone[]"  placeholder="Celular"/></p>
		            			<p class="col-md-12 semp"><input type="text"  class="painterinput telefone_pintor"  name="telefone[]"  placeholder="Telefone Fixo"/></p>
		            			<p class="col-md-12 semp"><input type="text"  class="painterinput email_pintor"  name="email[]"  placeholder="E-mail"/></p>
		            			<p class="col-md-12 semp"><input type="text"  class="painterinput profissao_pintor"  name="profissao[]"  placeholder="Profissão"/></p>	 
		            			<br clear="all"/>            			
		            		</div>
		            	</div>    
		            	<br clear="all"/>  
		            	<p class="col-md-12 semp"><input class="btnadd" type="button" data="{{$type}}" value="+Adicionar Participante"/></p>

		            	<h3>Upload de fotos</h3>
		            	<div class="col-md-12" style="padding:0px">
		            		<p class="col-md-12 semp">
		            			<input type="file" name="file" placeholder="Buscar Arquivo"/>
		            			<div class="img"></div>
		            		</p>
		            	</div>	


		            	<div class="col-md-12"><p class="pull-right"><input type="button" class="submitcurso" value="Enviar"/></p></div>
	            	</form>