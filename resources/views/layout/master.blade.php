<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Com o apoio e parceria de empresas que investem na capacitação profissional, criamos a Academia do pintor da Sherwin-Williams, um programa desenvolvido com o objetivo de formar, aperfeiçoar e profissionalizar pintores e trabalhadores da construção civil que estão iniciando na atividade de pintura.">
    <title>Academia do Pintor</title>
    <meta name="_token" content="{!! csrf_token() !!}" />
    <link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/bd1fdd65-0690-467d-8832-658d5d3c7398.css"/>
    <!-- Bootstrap core CSS -->
    <link href="css/style.css" rel="stylesheet">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
  </head>
  <body class="{{$bodyClass or ""}}">
    <div class="container">
      @include("partials.cookies")
      @include("partials.menu")    
      @yield("content")    
      @include("partials.modais")
      @include("partials.footer")
      @include("partials.scripts")
    </div>  
  </body>
</html>      