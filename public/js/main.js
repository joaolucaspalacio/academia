$(document).ready(function(){
    $('#select').customSelect();
});
$(document).ready(function() {
	

	//UF FILTER
	$('#select').on('change', function(e) {
		e.preventDefault();
		//VALUE OF SELECT
		$('div.box').addClass('hide');
		var id = $(this).val();

		if(id){
			$('div.'+id).removeClass('hide');
		}
	
		switch(id){
			case("SP"):
				$('h3').text("São Paulo");
			break;
			case("MG"):
				$('h3').text("Minas Gerais");
			break;
			case("RJ"):
				$('h3').text("Rio de Janeiro");
			break;
			case("DF"):
				$('h3').text("Distrito Federal");
			break;
			case("GO"):
				$('h3').text("Goiás");
			break;
			case("PE"):
				$('h3').text("Pernambuco");
			break;
			case("RN"):
				$('h3').text("Rio Grande do Norte");
			break;
			case("BA"):
				$('h3').text("Bahia");
			break;
			case("CE"):
				$('h3').text("Ceará");
			break;
			case("AL"):
				$('h3').text("Alagoas");
			break;
			case("PB"):
				$('h3').text("Paraíba");
			break;
			case("PR"):
				$('h3').text("Paraná");
			break;
			case("MS"):
				$('h3').text("Mato Grosso do Sul");
			break;
			default:
				$('h3').text("");
		}

		$('html, body').scrollTop($("#select").offset().top);

		//Event para o GA para monitor os estados selecionados
		ga('send','event', 'select', 'change', id);
	});
});