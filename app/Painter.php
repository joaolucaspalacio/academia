<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Painter extends Model
{
    //
    public function survey(){
    	 return $this->hasMany('App\Survey');
    }    
}
