<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    //

    public function pintores(){
    	 return $this->belongsTo('App\Painter');
    }
    public function cursos(){
    	 return $this->belongsTo('App\Curse');
    }
}
