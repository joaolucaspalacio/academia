<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use Validator;
use App\User;
use App\Survey;
use App\Curse;
use App\Painter;
use Input;  
use DB;
use Flash;
use Carbon\Carbon;
use Excel;
class AdminController extends Controller
{

    

    public function logout(){
        Auth::logout();
        return redirect()->route('login');
    }

    public function index(){
        return view('admin.home')->with(["model"=>Survey::get()]);;
    }
    
    public function pintores(){
        
        return view('admin.pintores')->with(["model"=>Painter::get()]);
    }
    
    public function eventos(){

        return view('admin.eventos')->with(["model"=>Curse::get()]);;
    }


    public function login(){
        return view('vendor.adminlte.login');
    }    

    public function doLogin(Request $request){
		if (Auth::attempt(['email' => $request->get("email"), 'password' => $request->get("password"), 'isAdmin' => 1], $request->get("remember"))) {
		    return redirect()->route('admin');
		}
		return Redirect::back()->withInput()->withErrors(["email"=>'Email ou senha incorretos.']);
    }    

  
}
