<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Curse;
use App\Painter;
use Input;
use App\Survey;
use Carbon\Carbon;
class ApiController extends Controller
{

    public function authenticate(Request $request)
    {

        if($request->ajax()){
            if (Auth::attempt(['email' => $request->get("email"), 'password' => $request->get("password")])) {
                // Authentication passed...
                 return response()->json([
                    'success' => '1',"name"=>Auth::user()->name
                ]);   
            }else{
                 return response()->json([
                    'success' => '0'
                ]);   
            }
        }
    }  

    public function loginpesquisa(Request $request){
        if($request->ajax()){
            $model = Painter::where("email",$request->get("email"))->first();
            return response()->json([
                "user"=>$model
            ]);
        }
    }

    public function home(){

        return view("index")->with(["gravado"=>""]);
    }

    public function sucesso(){

        return view("index")->with(["gravado"=>"1"]);
    }


    public function getCursos(){
        return response()->json([
            'cursos' => Curse::where("user_id",Auth::user()->id)->with("pintores")->get()
        ]); 
    }

    public function sendpesquisa(Request $request)
    {        
        $model = new Survey;
        $model->name = $request->get("name");
        $model->cep = $request->get("cep");
        $model->address = $request->get("address");
        $model->number = $request->get("number");
        $model->complemento = $request->get("complemento");
        $model->bairro = $request->get("bairro");
        $model->cidade = $request->get("cidade");
        $model->uf = $request->get("uf");
        $model->birth = $request->get("birth");
        $model->celphone = $request->get("celphone");
        $model->telefone = $request->get("telefone");
        $model->email = $request->get("email");
        $model->profissao = $request->get("profissao");

        $model->painter_id = $request->get("painter_id");
        $model->curse_id = $request->get("curse_id");
        $model->conteudo = $request->get("conteudo");
        $model->conhecimento = $request->get("conhecimento");
        $model->programa = $request->get("programa");
        $model->material = $request->get("material");
        $model->motivos = $request->get("motivos");
        $model->caract1 = $request->get("caract1");
        $model->caract2 = $request->get("caract2");
        $model->caract3 = $request->get("caract3");
        $model->alta1 = $request->get("alta1");
        $model->alta2 = $request->get("alta2");
        $model->opiniao = $request->get("opiniao");
        $model->save();
        flash('1', 'sucesso');
        return redirect()->route('home');             
    }

    public function getcursoid(Request $request){
        return response()->json([
            'obj' => Curse::with("pintores")->where("id",$request->get("id"))->where("user_id",Auth::user()->id)->first()
        ]);

    }

   public function curso(Request $request)
    {        
        if ($request->get("editid")!=""){
            $model = Curse::where("id",$request->get("editid"))->first();
            Painter::where('curse_id', $request->get("editid"))->delete();
        }else{
            $model = new Curse;    
        }
        $model->parceiro = $request->get("parceiro");
        $model->type = $request->get("tipo");
        $model->cidade = $request->get("cidadecurso");
        $model->user_id = Auth::user()->id;
        $model->participantes = $request->get("participantes");
        $model->dia = $request->get("dia")."/".$request->get("mes")."/".$request->get("ano");
        $model->unidade = $request->get("unidade");
        if($request->hasFile('file')) {
            $file = Input::file('file');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());        
            $name = $timestamp. '-' .$file->getClientOriginalName();            
            $model->foto = $name;
            $file->move(public_path().'/images/', $name);
        }         
        $model->save();
        if (count($request->get("name"))>0){
            for ($i=0; $i < count($request->get("name")); $i++) {                 
                if ($request->get("name")[$i]!="" && $request->get("email")[$i]!=""){
                    $painter = new Painter;
                    $painter->name = $request->get("name")[$i];
                    $painter->cep = $request->get("cep")[$i];
                    $painter->address = $request->get("address")[$i];
                    $painter->number = $request->get("number")[$i];
                    $painter->complemento = $request->get("complemento")[$i];
                    $painter->bairro = $request->get("bairro")[$i];
                    $painter->cidade = $request->get("cidade")[$i];
                    $painter->uf = $request->get("uf")[$i];
                    $painter->birth = $request->get("birth")[$i];
                    $painter->celphone = $request->get("celphone")[$i];
                    $painter->telefone = $request->get("telefone")[$i];
                    $painter->email = $request->get("email")[$i];
                    $painter->profissao = $request->get("profissao")[$i];
                    $painter->curse_id = $model->id;
                    $painter->save();
                }
            }
        }
        flash('1', 'sucesso');
        return redirect()->route('home');
    }   



    public function registerUser(Request $request){
    	if($request->ajax()){
    		$password = str_random(8);
    		$model = new User;
    		$model->name = $request->get("name");
    		$model->email = $request->get("email");
    		$model->rede = $request->get("rede");
    		$model->password = Hash::make($password);
    		$model->telefone = $request->get("ddd")." ".$request->get("telefone");
    		if ($model->save()){
                Mail::queue('emails.new_account', compact('password'), function ($message) use ($model) {
                    $message->subject('Sua nova conta na Academia do Pintor');
                    $message->to($model->email, $model->name);
                });
                Auth::login($model);
                return response()->json([
                    'success' => '1'
                ]);                         
            }else{
                return response()->json([
                    'success' => '0'
                ]);                         
            }
    	}
    }
}
