<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curse extends Model
{
    //

    public function pintores(){
    	 return $this->hasMany('App\Painter');
    }

    public function surveys(){
    	 return $this->hasMany('App\Survey');
    }
}
