<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ["as"=>"home","uses"=>"ApiController@home"]);

Route::get('/salvo', ["as"=>"sucesso","uses"=>"ApiController@sucesso"]);

Route::get('/user/register',["as"=>"register.user","uses"=>"ApiController@registerUser"]);
Route::get('/user/login',["as"=>"login.user","uses"=>"ApiController@authenticate"]);
Route::get('/pesquisa/login',["as"=>"login.pesquisa","uses"=>"ApiController@loginpesquisa"]);
Route::post('/pesquisa/send',["as"=>"send.pesquisa","uses"=>"ApiController@sendpesquisa"]);
Route::post('/cursos',["as"=>"gravacurso","uses"=>"ApiController@curso"]);
Route::post('/curso/',["as"=>"getCursoID","uses"=>"ApiController@getcursoid"]);
Route::get('/cursos',["as"=>"getcurso","uses"=>"ApiController@getCursos"]);
Route::get('/logout', ["as"=>"logout","uses"=>"AdminController@logout"]);


/*ADMIN*/
Route::group(['prefix' => '/admin','middleware' => 'isAdmin'], function () {
	Route::get('/', ["as"=>"home","uses"=>"AdminController@index"]);
	Route::get('/pintores', ["as"=>"pintores","uses"=>"AdminController@pintores"]);
	Route::get('/eventos', ["as"=>"eventos","uses"=>"AdminController@eventos"]);
});

Route::auth();