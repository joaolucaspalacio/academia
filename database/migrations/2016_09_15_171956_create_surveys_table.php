<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
          $table->integer('painter_id')->nullable();
          $table->integer('curse_id')->nullable();
          $table->string('conteudo')->nullable();
          $table->string('conhecimento')->nullable();
          $table->string('programa')->nullable();
          $table->string('material')->nullable();
          $table->string('motivos')->nullable();
          $table->string('caract1')->nullable();
          $table->string('caract2')->nullable();
          $table->string('caract3')->nullable();
          $table->string('alta1')->nullable();
          $table->string('alta2')->nullable();
          $table->string('opiniao')->nullable();  
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('telefone')->nullable();
            $table->string('cep')->nullable();
            $table->string('address')->nullable();
            $table->string('number')->nullable();
            $table->string('complemento')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('uf')->nullable();
          $table->string('birth')->nullable();
          $table->string('celphone')->nullable();
          $table->string('profissao')->nullable();                              
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
